package hu.plajko.adventofcode2020;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import com.google.common.base.Stopwatch;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Day24 implements CommandLineRunner {

    public static void main(String[] args) {
        var timer = Stopwatch.createStarted();
        SpringApplication.run(Day24.class, args);
        LOG.info("took {}", timer);
    }

    @AllArgsConstructor(staticName = "of")
    @EqualsAndHashCode
    static class Point {
        long x;
        long y;

        Point offset(long x, long y) {
            return Point.of(this.x + x, this.y + y);
        }
    }

    @Override
    public void run(String... args) throws Exception {
        var data = AdventUtils.readLines("day24.in");
        var paths = data.stream()
            .map(this::path)
            .collect(Collectors.toList());
        var tiles = paths.stream()
            .map(this::hexPoint)
            .collect(Collectors.groupingBy(
                Function.identity(),
                Collectors.counting()));

        var blackTiles = tiles.entrySet().stream()
            .filter(e -> e.getValue() %2 == 1)
            .map(Entry::getKey)
            .collect(Collectors.toSet());

        LOG.info("part1: {}", blackTiles.size());

        for (int i = 0; i < 100; i++) {
            blackTiles = nextDay(blackTiles);
        }

        LOG.info("part2: {}", blackTiles.size());
    }

    private Set<Point> nextDay(Set<Point> current) {
        var result = new HashSet<Point>();
        for (var tile : current) {
            int black = 0;
            for (var neighbour : neighbours(tile)) {
                if (current.contains(neighbour)) {
                    black++;
                } else if (2 == neighbours(neighbour).stream().filter(current::contains).count()) {
                    result.add(neighbour);
                }
            }
            if (black > 0 && black <= 2) {
                result.add(tile);
            }
        }
        return result;
    }

    private List<Point> neighbours(Point p){
        return List.of(
            p.offset(-1, 1),
            p.offset(0, 1),
            p.offset(1, 0),
            p.offset(1, -1),
            p.offset(0, -1),
            p.offset(-1, 0));
    }

    private List<String> path(String s) {
        var path = new ArrayList<String>();
        int i = 0;
        while (i < s.length()) {
            if (s.startsWith("sw", i) || s.startsWith("se", i) || s.startsWith("ne", i) || s.startsWith("nw", i)) {
                path.add(s.substring(i, i + 2));
                i += 2;
            } else if (s.startsWith("e", i) || s.startsWith("w", i)) {
                path.add(s.substring(i, i + 1));
                i += 1;
            }
        }
        return path;
    }

    private Point hexPoint(List<String> path) {
        var count = new HashMap<>(Map.of("sw", 0L, "se", 0L, "nw", 0L, "ne", 0L, "e", 0L, "w", 0L));
        count.putAll(path.stream().collect(Collectors.groupingBy(Function.identity(), Collectors.counting())));
        long x = count.get("e") + count.get("se") - count.get("w") - count.get("nw");
        long y = count.get("ne") + count.get("nw") - count.get("se") - count.get("sw");
        return Point.of(x, y);
    }

}
