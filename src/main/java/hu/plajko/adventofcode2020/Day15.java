package hu.plajko.adventofcode2020;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import com.google.common.collect.ListMultimap;
import com.google.common.collect.MultimapBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Day15 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day15.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        var lines = AdventUtils.readLines("day15.in");
        var data = lines.stream()
            .collect(Collectors.toList());

        var nums = Arrays.stream(data.get(0).split(",")).map(Long::parseLong).collect(Collectors.toList());

        LOG.info("part1: {}", solvePlajkoV1(nums, 2020));
        LOG.info("part1: {}", solveInspiredByPkovacs84(nums, 2020));
        LOG.info("part2: {}", solvePlajkoV1(nums, 30000000));
        LOG.info("part2: {}", solveInspiredByPkovacs84(nums, 30000000));
    }

    long solveInspiredByPkovacs84(List<Long> init, int maxTurns) {
        Map<Long, Long> m = new HashMap<>(maxTurns);
        long turn = 1;
        for (int i = 0; i < init.size() - 1; i++) {
            m.put(init.get(i), turn);
            turn++;
        }
        long last = init.get(init.size() - 1);
        while (turn < maxTurns) {
            var occ = m.get(last);
            m.put(last, turn);
            last = occ == null ? 0 : turn - occ;
            turn++;
        }
        return last;
    }

    long solvePlajkoV1(List<Long> init, int maxTurns) {
        ListMultimap<Long, Integer> m = MultimapBuilder.hashKeys().arrayListValues().build();
        int turn = 1;
        long last = 0;
        for (int i = 0; i < init.size(); i++) {
            m.put(init.get(i), turn);
            last = init.get(i);
            turn++;
        }
        while (turn <= maxTurns) {
            var occ = m.get(last);
            if (occ.size() == 1) {
                m.put(0L, turn);
                last = 0L;
            } else {
                long num = occ.get(occ.size() - 1) - occ.get(occ.size() - 2);
                m.put(num, turn);
                last = num;
            }
            turn++;
        }
        return last;
    }
}
