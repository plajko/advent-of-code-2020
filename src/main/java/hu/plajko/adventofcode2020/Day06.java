package hu.plajko.adventofcode2020;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import com.google.common.base.Splitter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Day06 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day06.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        var lines = AdventUtils.readLines("day06.in");
        var part1 = parseBlocks(lines)
            .mapToLong(s ->  s.stream()
                .flatMapToInt(String::chars)
                .distinct()
                .count())
            .sum();
        LOG.info("part1: {}", part1);

        var part2 = parseBlocks(lines)
            .mapToLong(s ->  s.stream()
                .flatMapToInt(String::chars).boxed()
                .collect(Collectors.groupingBy(
                    Function.identity(),
                    Collectors.counting()))
                .entrySet().stream()
                .filter(e -> e.getValue() == s.size())
                .count())
            .sum();
        LOG.info("part2: {}", part2);
    }

    private Stream<List<String>> parseBlocks(List<String> lines) {
        return AdventUtils.split(lines, String::isBlank).stream()
            .map(blocks -> blocks.stream()
                .flatMap(Splitter.on(" ")::splitToStream)
                .collect(Collectors.toList()));
    }
}
