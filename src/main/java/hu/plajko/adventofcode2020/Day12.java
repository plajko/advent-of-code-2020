package hu.plajko.adventofcode2020;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import com.google.common.base.Preconditions;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Day12 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day12.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        var lines = AdventUtils.readLines("day12.in");

        var data = lines.stream().collect(Collectors.toList());

        LOG.info("part1: {}", solve1(data));
        LOG.info("part2: {}", solve2(data));
    }

    private long solve1(List<String> data) {
        var dirs = List.of("N", "E", "S", "W");
        long x = 0;
        long y = 0;
        var current = "E";
        for (int i = 0; i < data.size(); i++) {
            var d = data.get(i).substring(0, 1);
            var n = Long.parseLong(data.get(i).substring(1));
            if (("R".equals(d) && n == 90) || ("L".equals(d) && n == 270)) {
                current = dirs.get((dirs.indexOf(current) + 1) % 4);
            } else if (("R".equals(d) && n == 270) || ("L".equals(d) && n == 90)) {
                current = dirs.get((dirs.indexOf(current) + 3) % 4);
            } else if (n == 180) {
                current = dirs.get((dirs.indexOf(current) + 2) % 4);
            } else {
                var dir = dirs.contains(d) ? d : "F".equals(d) ? current : null;
                Preconditions.checkState(dir != null);
                if ("N".equals(dir)) {
                    y -= n;
                } else if ("S".equals(dir)) {
                    y += n;
                } else if ("E".equals(dir)) {
                    x -= n;
                } else if ("W".equals(dir)) {
                    x += n;
                }
            }
            //            LOG.info("{} {} ({} {})", x, y);
        }
        return Math.abs(x) + Math.abs(y);
    }

    private long solve2(List<String> data) {
        long x = -10;
        long y = -1;
        long sx = 0;
        long sy = 0;
        for (int i = 0; i < data.size(); i++) {
            var d = data.get(i).substring(0, 1);
            var n = Long.parseLong(data.get(i).substring(1));
            long a = y;
            if (("R".equals(d) && n == 90) || ("L".equals(d) && n == 270)) {
                y = -x;
                x = a;
            } else if (("R".equals(d) && n == 270) || ("L".equals(d) && n == 90)) {
                y = x;
                x = -a;
            } else if (n == 180) {
                y = -y;
                x = -x;
            } else if ("F".equals(d)) {
                sx += n * x;
                sy += n * y;
            } else if ("N".equals(d)) {
                y -= n;
            } else if ("S".equals(d)) {
                y += n;
            } else if ("E".equals(d)) {
                x -= n;
            } else if ("W".equals(d)) {
                x += n;
            } else
                throw new IllegalStateException(data.get(i) + "!");
            //            LOG.info("{} {} ({} {})", sx, sy, x, y);
        }
        return Math.abs(sx) + Math.abs(sy);
    }

}
