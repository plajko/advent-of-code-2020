package hu.plajko.adventofcode2020;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import com.google.common.base.Stopwatch;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Day22 implements CommandLineRunner {

    public static void main(String[] args) {
        var timer = Stopwatch.createStarted();
        SpringApplication.run(Day22.class, args);
        LOG.info("took {}", timer);
    }

    @Override
    public void run(String... args) throws Exception {
        var data = AdventUtils.readLines("day22.in");
        var blocks = new ArrayList<>(AdventUtils.split(data, String::isEmpty));
        var p1 = blocks.get(0);
        p1.remove(0);
        var p2 = blocks.get(1);
        p2.remove(0);
        var p1cards = p1.stream().map(Integer::parseInt).collect(Collectors.toList());
        var p2cards = p2.stream().map(Integer::parseInt).collect(Collectors.toList());
        LOG.info("part1: {}", score(p1cards, p2cards, false));
        LOG.info("part2: {}", score(p1cards, p2cards, true));
    }

    long score(List<Integer> p1, List<Integer> p2, boolean advanced) {
        var state = play(p1, p2, advanced);
        var result = new ArrayList<>(state.get(0));
        result.addAll(state.get(1));
        return IntStream.rangeClosed(1, result.size()).mapToLong(i -> i * result.get(result.size() - i)).sum();
    }

    List<Collection<Integer>> play(List<Integer> p1cards, List<Integer> p2cards, boolean advanced) {
        var p1 = new ArrayDeque<>(p1cards);
        var p2 = new ArrayDeque<>(p2cards);
        var cache = new HashSet<List<Integer>>();
        while (!p1.isEmpty() && !p2.isEmpty()) {
            if (advanced && (!cache.add(List.copyOf(p1)) || !cache.add(List.copyOf(p2)))) {
                return List.of(p1, List.of());
            }
            int n1 = p1.removeFirst();
            int n2 = p2.removeFirst();
            boolean p1Wins = n1 > n2;
            if (advanced && p1.size() >= n1 && p2.size() >= n2) {
                p1Wins = !play(List.copyOf(p1).subList(0, n1), List.copyOf(p2).subList(0, n2), advanced).get(0).isEmpty();
            }
            if (p1Wins) {
                p1.add(n1);
                p1.add(n2);
            } else {
                p2.add(n2);
                p2.add(n1);
            }
        }
        return List.of(p1, p2);
    }
}
