package hu.plajko.adventofcode2020;

import static java.util.stream.LongStream.rangeClosed;

import java.util.TreeSet;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Day05 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day05.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        var ids = AdventUtils.readLines("day05.in").stream()
            .map(s -> Long.parseLong(s.replaceAll("[BR]", "1").replaceAll("[FL]", "0"), 2))
            .collect(Collectors.toCollection(TreeSet::new));
        LOG.info("part1 (max): {}", ids.last());
        LOG.info("part2 (missing): {}", rangeClosed(ids.first(), ids.last()).boxed()
            .filter(Predicate.not(ids::contains))
            .findFirst()
            .orElseThrow());
    }
}
