package hu.plajko.adventofcode2020;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import com.google.common.base.Stopwatch;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Day23 implements CommandLineRunner {

    public static void main(String[] args) {
        var timer = Stopwatch.createStarted();
        SpringApplication.run(Day23.class, args);
        LOG.info("took {}", timer);
    }

    @Override
    public void run(String... args) throws Exception {
        var data = AdventUtils.readLines("day23.in");
        var cups = data.get(0).chars().mapToObj(c -> Integer.parseInt(Character.toString(c))).collect(Collectors.toList());

        var circle1 = createLinkedArray(cups);
        int max1 = cups.stream().mapToInt(i -> i).max().getAsInt();

        playGame(circle1, cups.get(0), 100, max1);
        StringBuilder sb = new StringBuilder();
        int c = circle1[1];
        while (sb.length() < cups.size() - 1) {
            sb.append(c);
            c = circle1[c];
        }
        LOG.info("part1: {}", sb.toString());

        for (int i = 10; i <= 1000000; i++) {
            cups.add(i);
        }

        var circle2 = createLinkedArray(cups);
        int max2 = cups.stream().mapToInt(i -> i).max().getAsInt();
        playGame(circle2, cups.get(0), 10000000, max2);

        int a = circle2[1];
        int b = circle2[a];
        LOG.info("part2: {}", (long) a * b);
    }

    private int[] createLinkedArray(List<Integer> list) {
        int[] result = new int[list.size() + 1];
        for (int i = 0; i < list.size(); i++) {
            result[list.get(i)] = list.get(i == list.size() - 1 ? 0 : i + 1);
        }
        return result;
    }

    private void playGame(int[] circle, int start, int rounds, int max) {
        int current = start;
        for (int round = 0; round < rounds; round++) {
            int p1 = circle[current];
            int p2 = circle[p1];
            int p3 = circle[p2];
            int next = circle[p3];
            circle[current] = next;

            int destination = current;
            while (destination == current || p1 == destination || p2 == destination || p3 == destination) {
                if (--destination < 1) {
                    destination = max;
                }
            }

            int afterDest = circle[destination];
            circle[destination] = p1;
            circle[p3] = afterDest;
            current = next;
        }
    }
}
