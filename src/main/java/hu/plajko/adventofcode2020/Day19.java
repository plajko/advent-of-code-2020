package hu.plajko.adventofcode2020;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Day19 implements CommandLineRunner {

    public static void main(String[] args) {
        var timer = Stopwatch.createStarted();
        SpringApplication.run(Day19.class, args);
        LOG.info("took {}", timer);
    }

    @Override
    public void run(String... args) throws Exception {
        var data = AdventUtils.readLines("day19.in");
        var blocks = Lists.newArrayList(AdventUtils.split(data, String::isEmpty));
        Map<Integer, String> rules = new HashMap<>();
        var ruleBlock = blocks.get(0);
        for (int i = 0; i < ruleBlock.size(); i++) {
            var s = ruleBlock.get(i).split(": ");
            int index = Integer.parseInt(s[0]);
            rules.put(index, s[1]);
        }

        var rule0 = Pattern.compile(getPattern(rules, 0));
        var part1 = blocks.get(1).stream().filter(s -> rule0.matcher(s).matches()).count();
        LOG.info("part1: {}", part1);

        var rule42 = Pattern.compile(getPattern(rules, 42));
        var rule31 = Pattern.compile(getPattern(rules, 31));
        var part2 = blocks.get(1).stream().filter(s -> matchesPart2(s, rule42, rule31)).count();
        LOG.info("part2: {}", part2);
    }

    /**
     * Matching rule 0 with new rules introduced.
     * Using tricks:
     *   - rule 0 is "0: 8 11"
     *   - rules 8, 11 are not referenced anywhere else
     */
    boolean matchesPart2(String s, Pattern rule42, Pattern rule31) {
        var m = rule42.matcher(s);
        int count42 = 0;
        int count31 = 0;
        while (m.lookingAt()) {
            m.region(m.end(), s.length());
            count42++;
        }
        m.usePattern(rule31);
        while (m.lookingAt()) {
            m.region(m.end(), s.length());
            count31++;
        }
        return m.hitEnd() && count42 > 1 && count31 > 0 && count31 < count42;
    }

    String getPattern(Map<Integer, String> rules, int index) {
        var first = rules.get(index);
        var split = first.split(" \\| ");
        if (!split[0].startsWith("\"")) {
            var pattern = Arrays.stream(split)
                .map(s -> Arrays.stream(s.split(" "))
                     .map(Integer::parseInt)
                     .map(ri -> getPattern(rules, ri))
                     .collect(Collectors.joining()))
                .collect(Collectors.joining("|"));
            return "(" + pattern + ")";
        } else {
            return split[0].substring(1, split[0].length() - 1);
        }
    }
}
