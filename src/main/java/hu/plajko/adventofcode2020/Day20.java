package hu.plajko.adventofcode2020;

import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Slf4j
public class Day20 implements CommandLineRunner {

    private static final String MONSTER =
        "                  # \n" +
        "#    ##    ##    ###\n" +
        " #  #  #  #  #  #   ";

    public static void main(String[] args) {
        var timer = Stopwatch.createStarted();
        SpringApplication.run(Day20.class, args);
        LOG.info("took {}", timer);
    }

    static class Tile {

        @Getter
        long id;
        private char[][] image;

        @Getter Tile[] neighbours = new Tile[4];

        Tile(long id, List<String> lines) {
            this.id = id;
            this.image = new char[lines.size()][lines.get(0).length()];
            for (int x = 0; x < image.length; x++) {
                for (int y = 0; y < image[0].length; y++) {
                    image[x][y] = lines.get(x).charAt(y);
                }
            }
        }

        Tile(long id, char[][] image) {
            this.id = id;
            this.image = image;
        }

        String dump() {
            return Arrays.stream(image).map(String::new).collect(Collectors.joining("\n"));
        }

        long countMatchedNeighbours() {
            return Arrays.stream(neighbours).filter(Objects::nonNull).count();
        }

        void rotate() {
            var result = new char[image[0].length][image.length];
            for (int x = 0; x < image.length; x++) {
                for (int y = 0; y < image[0].length; y++) {
                    result[image[0].length - 1 - y][x] = image[x][y];
                }
            }
            this.image = result;
        }

        void flip() {
            var result = new char[image[0].length][image.length];
            for (int x = 0; x < image.length; x++) {
                for (int y = 0; y < image[0].length; y++) {
                    result[image.length - 1 - x][y] = image[x][y];
                }
            }
            this.image = result;
        }

        List<String> edges() {
            var charCollector = Collector.of(
                StringBuilder::new,
                StringBuilder::append,
                StringBuilder::append,
                Function.identity());
            var result = new ArrayList<String>();
            result.add(new String(image[0]));
            result.add(Arrays.stream(image).map(row -> row[image[0].length - 1]).collect(charCollector).toString());
            result.add(new StringBuilder(new String(image[image.length - 1])).reverse().toString());
            result.add(Arrays.stream(image).map(row -> row[0]).collect(charCollector).reverse().toString());
            return result;
        }

        char[][] withoutEdges() {
            var result = new char[image.length - 2][image[0].length - 2];
            for (int x = 0; x < result.length; x++) {
                for (int y = 0; y < result[0].length; y++) {
                    result[x][y] = image[x + 1][y + 1];
                }
            }
            return result;
        }

        List<int[]> fullMatches(char[][] pattern) {
            List<int[]> matches = new ArrayList<>();
            for (int x = 0; x < image.length - pattern.length; x++) {
                o: for (int y = 0; y < image[0].length - pattern[0].length; y++) {
                    for (int i = 0; i < pattern.length; i++) {
                        for (int j = 0; j < pattern[0].length; j++) {
                            if (pattern[i][j] != '#') {
                                continue;
                            }
                            if (image[x + i][y + j] != '#') {
                                continue o;
                            }
                        }
                    }
                    matches.add(new int[] { x, y });
                }
            }
            return matches;
        }

        Tile apply(List<int[]> positions, char[][] pattern) {
            var result = new char[image.length][];
            for (int i = 0; i < image.length; i++) {
                result[i] = image[i].clone();
            }
            for (var pos : positions) {
                for (int i = 0; i < pattern.length; i++) {
                    for (int j = 0; j < pattern[0].length; j++) {
                        result[pos[0] + i][pos[1] + j] = pattern[i][j] == '#' ? 'O' : ' ' ;
                    }
                }
            }
            return new Tile(id, result);
        }

        long count() {
            return Arrays.stream(image).mapToLong(r -> new String(r).chars().filter(c -> '#' == c).count()).sum();
        }
    }

    @Override
    public void run(String... args) throws Exception {
        var data = AdventUtils.readLines("day20.in");

        var blocks = Lists.newArrayList(AdventUtils.split(data, String::isEmpty));
        var tiles = new ArrayList<Tile>();
        for (int i = 0; i < blocks.size(); i++) {
            String id = blocks.get(i).remove(0).substring(5).replaceFirst(":$", "");
            tiles.add(new Tile(Long.parseLong(id), blocks.get(i)));
        }

        Multimap<Tile, Tile> adjacency = MultimapBuilder.hashKeys().hashSetValues().build();
        for (Tile t1 : tiles) {
            for (Tile t2 : tiles) {
                if (t1.getId() == t2.getId()) {
                    continue;
                }
                for (var e1 : t1.edges()) {
                    for (var e2 : t2.edges()) {
                        if (e1.equals(e2)) {
                            adjacency.put(t1, t2);
                        }
                    }
                    t2.flip();
                    for (var e2 : t2.edges()) {
                        if (e1.equals(e2)) {
                            adjacency.put(t1, t2);
                        }
                    }
                }
            }
        }

        var corners = adjacency.asMap().entrySet().stream()
            .filter(e -> e.getValue().size() == 2)
            .map(Entry::getKey)
            .collect(Collectors.toList());

        LOG.info("part1: {}", corners.stream().map(Tile::getId).reduce(1L, (a, b) -> a * b));

        var size = (int) Math.sqrt(blocks.size());

        var insideTiles = adjacency.asMap().entrySet().stream()
            .filter(e -> e.getValue().size() == 4)
            .map(Entry::getKey)
            .collect(Collectors.toList());
        var start = insideTiles.get(0);
        matchNeighbours(start, adjacency);
        var topLeft = Iterables.getOnlyElement(tiles.stream()
            .filter(t ->
                   t.getNeighbours()[1] != null
                && t.getNeighbours()[2] != null
                && t.getNeighbours()[3] == null
                && t.getNeighbours()[0] == null)
            .collect(Collectors.toList()));

        char[][] bigImage = new char[size * 8][size * 8];
        var rowStart = topLeft;
        for (int i = 0; i < size; i++) {
            var cur = rowStart;
            for (int j = 0; j < size; j++) {
                var withoutEdges = cur.withoutEdges();
                for (int x = 0; x < withoutEdges.length; x++) {
                    for (int y = 0; y < withoutEdges[0].length; y++) {
                        bigImage[i * 8 + x][j * 8 + y] = withoutEdges[x][y];
                    }
                }
                cur = cur.getNeighbours()[1];
            }
            Preconditions.checkState(cur == null);
            rowStart = rowStart.getNeighbours()[2];
        }
        Preconditions.checkState(rowStart == null);

        var fullImage = new Tile(0, bigImage);
//        System.out.println(fullImage.dump());

        char[][] monster = MONSTER.lines().map(String::toCharArray).toArray(char[][]::new);
        long monsterCount = MONSTER.chars().filter(c -> '#' == c).count();

        for (int j = 0; j < 8; j++) {
            var monsters = fullImage.fullMatches(monster);
            if (monsters.size() != 0) {
                LOG.info("part2: {}", fullImage.count() - monsters.size() * monsterCount);
//                System.out.println(fullImage.apply(monsters, monster).dump());
            }
            fullImage.rotate();
            if (j == 3) {
                fullImage.flip();
            }
        }
    }

    static void matchNeighbours(Tile tile, Multimap<Tile, Tile> adjacency) {
        var neighbours = adjacency.get(tile);
        if (neighbours.size() == tile.countMatchedNeighbours()) {
            return;
        }
        for (var neighbour : neighbours) {
            int edge = 0;
            while (edge < 4) {
                if (align(tile, edge, neighbour)) {
                    break;
                }
                edge++;
            }
            if (tile.getNeighbours()[edge] != null) {
                Preconditions.checkState(neighbour.equals(tile.getNeighbours()[edge]));
            }
            tile.getNeighbours()[edge] = neighbour;
            neighbour.getNeighbours()[(edge + 2) % 4] = tile;
        }
        neighbours.forEach(n -> matchNeighbours(n, adjacency));
    }

    static boolean align(Tile target, int edge, Tile tile) {
        String match = target.edges().get(edge);
        int opposite = (edge + 2) % 4;
        boolean matched = false;
        for (int j = 0; j < 8; j++) {
            String candidate = new StringBuilder(tile.edges().get(opposite)).reverse().toString();
            if (match.equals(candidate)) {
                matched = true;
                break;
            }
            tile.rotate();
            if (j == 3) {
                tile.flip();
            }
        }
        return matched;
    }

}
