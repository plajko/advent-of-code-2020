package hu.plajko.adventofcode2020;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Iterators;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import com.google.common.collect.Sets;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Day21 implements CommandLineRunner {

    public static void main(String[] args) {
        var timer = Stopwatch.createStarted();
        SpringApplication.run(Day21.class, args);
        LOG.info("took {}", timer);
    }

    @Data
    @AllArgsConstructor
    static class Food {
        Set<String> ingredients;
        Set<String> allergenes;
    }

    @Override
    public void run(String... args) throws Exception {
        var data = AdventUtils.readLines("day21.in");

        ListMultimap<String, List<String>> allergeneIndex = MultimapBuilder.hashKeys().arrayListValues().build();
        var foods = new ArrayList<Food>();
        for (int i = 0; i < data.size(); i++) {
            var s = data.get(i).split(" \\(contains ");
            var ingredients = Sets.newHashSet(s[0].split(" "));
            var allergenes = Sets.newHashSet(s[1].replaceFirst("\\)$", "").split(", "));
            var food = new Food(ingredients, allergenes);
            foods.add(food);
            for (var allergene : allergenes) {
                allergeneIndex.put(allergene, Lists.newArrayList(food.getIngredients()));
            }
        }
        for (var allergene : allergeneIndex.keySet()) {
            var ingredients = allergeneIndex.get(allergene);
            for (int i = 0; i < ingredients.size() - 1; i++) {
                for (int j = i + 1; j < ingredients.size(); j++) {
                    ingredients.get(i).retainAll(ingredients.get(j));
                    ingredients.get(j).retainAll(ingredients.get(i));
                }
            }
        }
        var possibleIngredients = allergeneIndex.values().stream()
            .flatMap(List::stream)
            .collect(Collectors.toList());
        var notPossibleIngredients = foods.stream().flatMap(f -> f.getIngredients().stream())
            .filter(ingredient -> !possibleIngredients.contains(ingredient))
            .collect(Collectors.toSet());

        long part1 = foods.stream()
            .map(Food::getIngredients)
            .flatMap(Collection::stream)
            .filter(notPossibleIngredients::contains)
            .count();
        LOG.info("part1: {}", part1);

        foods.forEach(food -> food.getIngredients().removeAll(notPossibleIngredients));
        Multimap<String, Set<String>> cleanAllergenIndex = MultimapBuilder.hashKeys().hashSetValues().build();
        foods.forEach(food -> food.getAllergenes()
            .forEach(allergene -> cleanAllergenIndex.put(allergene, food.getIngredients())));

        Multimap<String, String> candidates = MultimapBuilder.hashKeys().hashSetValues().build();
        cleanAllergenIndex.asMap().forEach((allergene, allFoodIngredients) -> {
            var countOccurances = allFoodIngredients.stream().flatMap(Collection::stream)
                  .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                  .entrySet().stream()
                  .sorted(Entry.<String, Long>comparingByValue().reversed())
                  .collect(Collectors.toList());
            long highest = countOccurances.get(0).getValue();
            var highestValueOnly = countOccurances.stream().filter(o -> o.getValue() == highest).collect(Collectors.toList());
            candidates.putAll(allergene, highestValueOnly.stream().map(Entry::getKey).collect(Collectors.toList()));
        });

        var matches = new TreeMap<String, String>();
        while (true) {
            var match = candidates.asMap().entrySet().stream()
                .filter(e -> e.getValue().stream().filter(c -> !matches.containsValue(c)).count() == 1)
                .findFirst();
            if (match.isEmpty()) {
                break;
            }
            var matchedAllergene = match.get().getKey();
            var matchedIngredient = Iterators.getOnlyElement(
                match.get().getValue().stream()
                    .filter(c -> !matches.containsValue(c))
                    .iterator());
            matches.put(matchedAllergene, matchedIngredient);
        }
        var part2 = matches.entrySet().stream().map(Entry::getValue).collect(Collectors.joining(","));
        LOG.info("part2: {}", part2);
    }

}
