package hu.plajko.adventofcode2020;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import java.math.BigInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Slf4j
public class Day13 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day13.class, args);
    }

    @Data
    @AllArgsConstructor(staticName = "of")
    static class CRTItem {
        private BigInteger number;
        private BigInteger remainder;
    }

    @Override
    public void run(String... args) throws Exception {
        var lines = AdventUtils.readLines("day13.in");
        var data = lines.stream()
            .collect(Collectors.toList());
        var start = Long.parseLong(data.get(0));
        var schedule = data.get(1).split(",");

        var periods = IntStream.range(0, schedule.length)
            .filter(i -> !"x".equals(schedule[i]))
            .mapToObj(i -> Long.parseLong(schedule[i]))
            .collect(Collectors.toList());
        long timestamp = start;
        final long part1Solution;
        while (true) {
            final long t = timestamp;
            var mod = periods.stream().filter(p -> t % p == 0).findFirst();
            if (mod.isPresent()) {
                part1Solution = (timestamp - start) * mod.get();
                break;
            }
            timestamp++;
        }
        LOG.info("part1: {}", part1Solution);

        var list = IntStream.range(0, schedule.length)
			.filter(i -> !"x".equals(schedule[i]))
			.mapToObj(i -> CRTItem.of(new BigInteger(schedule[i]), BigInteger.valueOf(-i)))
            .collect(Collectors.toList());

        // calculate with Chinese Remainder Theorem
        // https://rosettacode.org/wiki/Chinese_remainder_theorem
        var product = list.stream()
            .map(CRTItem::getNumber)
            .reduce(BigInteger.ONE, BigInteger::multiply);
        var partialProduct = list.stream()
            .map(CRTItem::getNumber)
            .map(product::divide)
            .collect(Collectors.toList());
        var sum = IntStream.range(0, list.size())
            .mapToObj(i -> partialProduct.get(i)
                .multiply(partialProduct.get(i).modInverse(list.get(i).getNumber()))
                .multiply(list.get(i).getRemainder()))
            .reduce(BigInteger.ZERO, BigInteger::add);
        var part2Solution = sum.mod(product);
        LOG.info("part2: {}", part2Solution);
    }
}
