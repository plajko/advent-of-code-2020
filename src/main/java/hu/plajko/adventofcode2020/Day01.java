package hu.plajko.adventofcode2020;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Day01 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day01.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        var numbers = AdventUtils.readLines("day01.in").stream()
            .map(BigInteger::new)
            .collect(Collectors.toCollection(TreeSet::new));
        printSolution(numbers, BigInteger.valueOf(2020), 2, "part1");
        printSolution(numbers, BigInteger.valueOf(2020), 3, "part2");
    }

    private static void printSolution(Set<BigInteger> numbers, BigInteger sum, int count, String label) {
        var solution = solve(numbers, sum, count);
        LOG.info("{}: {} {}", label, solution.stream().reduce(BigInteger.ONE, BigInteger::multiply), solution);
    }

    private static List<BigInteger> solve(Set<BigInteger> numbers, BigInteger sum, int count) {
        if (count <= 2) {
            return numbers.stream().parallel()
                .filter(n -> numbers.contains(sum.subtract(n)))
                .findFirst()
                .map(n -> Lists.newArrayList(n, sum.subtract(n)))
                .orElse(Lists.newArrayList());
        } else {
            for (BigInteger n : numbers) {
                if (sum.subtract(n).compareTo(BigInteger.TWO) < 0) {
                    continue;
                }
                var s = solve(numbers, sum.subtract(n), count - 1);
                if (!s.isEmpty()) {
                    s.add(n);
                    return s;
                }
            }
            return Lists.newArrayList();
        }
    }

}
