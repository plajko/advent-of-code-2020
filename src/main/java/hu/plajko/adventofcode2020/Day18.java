package hu.plajko.adventofcode2020;

import java.util.regex.Pattern;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import com.google.common.base.Stopwatch;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Day18 implements CommandLineRunner {

    public static void main(String[] args) {
        var timer = Stopwatch.createStarted();
        SpringApplication.run(Day18.class, args);
        LOG.info("took {}", timer);
    }

    @Override
    public void run(String... args) throws Exception {
        var data = AdventUtils.readLines("day18.in");
        long part1Sum = 0;
        long part2Sum = 0;
        for (int i = 0; i < data.size(); i++) {
            part1Sum += evaluate(data.get(i), false);
            part2Sum += evaluate(data.get(i), true);
        }
        LOG.info("part1: {}", part1Sum);
        LOG.info("part2: {}", part2Sum);
    }

    private long evaluate(String expression, boolean addBeforeMultiplication) {
        var p = Pattern.compile("\\(([^\\(\\)]*)\\)");
        var m = p.matcher(expression);
        var sb = new StringBuilder();
        while (m.find()) {
            m.appendReplacement(sb, Long.toString(evaluateWithoutParentheses(m.group(1), addBeforeMultiplication)));
        }
        m.appendTail(sb);
        var replaced = sb.toString();
        if (replaced.contains("(")) {
            return evaluate(replaced, addBeforeMultiplication);
        } else {
            return evaluateWithoutParentheses(replaced, addBeforeMultiplication);
        }
    }

    private long evaluateWithoutParentheses(String e, boolean addBeforeMultiplication) {
        if (addBeforeMultiplication && e.contains("+") && e.contains("*")) {
            var p = Pattern.compile("(\\d+ \\+ \\d+)");
            var m = p.matcher(e);
            var sb = new StringBuilder();
            while (m.find()) {
                m.appendReplacement(sb, "(" + m.group(1) + ")");
            }
            m.appendTail(sb);
            return evaluate(sb.toString(), addBeforeMultiplication);
        }
        var s = e.split(" ");
        Long result = null;
        String op = null;
        for (int i = 0; i < s.length; i++) {
            if (s[i].matches("\\d+")) {
                var n = Long.parseLong(s[i]);
                if (result == null) {
                    result = n;
                } else {
                    if (op.equals("*")) {
                        result *= n;
                    } else if (op.equals("+")) {
                        result += n;
                    }
                }
            } else if (s[i].equals("*")) {
                op = s[i];
            } else if (s[i].equals("+")) {
                op = s[i];
            }
        }
        return result;
    }

}
