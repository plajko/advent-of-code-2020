package hu.plajko.adventofcode2020;

import java.util.Arrays;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import com.google.common.collect.Maps;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Day07 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day07.class, args);
    }

    @Data
    @AllArgsConstructor(staticName = "of")
    static class Bag {
        String color;
        Map<String, Long> bags;
    }

    @AllArgsConstructor(staticName = "of")
    static class Bags {
        Map<String, Bag> bags;

        Stream<Bag> containsRecursive(String color) {
            return bags.values().stream().filter(contains(color));
        }
        private Predicate<Bag> contains(String color) {
            return bag -> bag.getBags().entrySet().stream().anyMatch(b -> contains(b, color));
        }
        private boolean contains(Entry<String, Long> bag, String color) {
            return bag.getKey().equals(color) ? true : contains(color).test(bags.get(bag.getKey()));
        }
        long countAllIn(String color) {
            return bags.get(color).getBags().entrySet().stream().mapToLong(this::countAllIn).sum();
        }
        private long countAllIn(Entry<String, Long> content) {
            return (1 + countAllIn(content.getKey())) * content.getValue();
        }
    }

    @Override
    public void run(String... args) throws Exception {
        var bagContentPattern = Pattern.compile("^(\\d+) (.*) bags?$");
        var lines = AdventUtils.readLines("day07.in");
        var bags = Bags.of(lines.stream()
             .map(line -> line.split(" bags? contain "))
             .map(bagDefinition -> {
                 var bagContent = bagDefinition[1].replaceAll("\\.$", "").split(", ");
                 return Bag.of(
                     bagDefinition[0],
                     bagContent[0].startsWith("no other bags") ? Map.of() : Arrays.stream(bagContent)
                         .map(bagContentPattern::matcher)
                         .map(m -> Optional.of(m).filter(Matcher::matches).orElseThrow())
                         .map(m -> Maps.immutableEntry(m.group(2), Long.parseLong(m.group(1))))
                         .collect(Collectors.toMap(Entry::getKey, Entry::getValue))
                 );
            })
            .collect(Collectors.toMap(Bag::getColor, Function.identity())));
        LOG.info("part1: {}", bags.containsRecursive("shiny gold").count());
        LOG.info("part2: {}", bags.countAllIn("shiny gold"));
    }

}
