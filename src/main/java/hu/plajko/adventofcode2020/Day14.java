package hu.plajko.adventofcode2020;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Day14 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day14.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        var lines = AdventUtils.readLines("day14.in");
        var data = lines.stream()
            .collect(Collectors.toList());

        var part1Mem = new HashMap<Long, Long>();
        var part2Mem = new HashMap<Long, Long>();
        var mask = "";
        for (int i = 0; i < data.size(); i++) {
            var line = data.get(i);
            if (line.startsWith("mask")) {
                mask = line.split(" = ")[1];
            } else {
                var split = line.split(" = ");
                var address = Long.parseLong(split[0].replaceAll("mem\\[", "").replaceAll("\\]", ""));
                long value = Long.parseLong(split[1]);

                // part1
                long maskedValue = applyMask(value, mask);
                if (maskedValue != 0) {
                    part1Mem.put(address, maskedValue);
                }

                // part2
                if (value != 0) {
                    floatingAdresses(address, mask).forEach(a -> part2Mem.put(a, value));
                }
            }
        }
        LOG.info("part1: {}", part1Mem.values().stream().mapToLong(Long::longValue).sum());
        LOG.info("part2: {}", part2Mem.values().stream().mapToLong(Long::longValue).sum());
    }

    private List<Long> floatingAdresses(Long address, String mask) {
        char[] init = new char[mask.length()];
        Arrays.fill(init, '0');
        var binaryAddress = Long.toString(address, 2).toCharArray();
        System.arraycopy(binaryAddress, 0, init, init.length - binaryAddress.length, binaryAddress.length);
        for (int i = 0; i < mask.length(); i++) {
            char maskBit = mask.charAt(i);
            if (maskBit == '1') {
                init[i] = '1';
            }
        }
        var results = new ArrayList<char[]>();
        results.add(init);
        for (int i = 0; i < mask.length(); i++) {
            final int j = i;
            char maskBit = mask.charAt(j);
            if (maskBit == 'X') {
                var temp = results.stream()
                    .map(a -> a.clone())
                    .collect(Collectors.toList());
                temp.forEach(a -> a[j] = '1');
                results.forEach(a -> a[j] = '0');
                results.addAll(temp);
            }
        }
        return results.stream()
            .map(chars -> Long.parseLong(new String(chars), 2))
            .collect(Collectors.toList());
    }

    private long applyMask(long value, String mask) {
        var result = new char[mask.length()];
        Arrays.fill(result, '0');
        var binaryValue = Long.toString(value, 2).toCharArray();
        System.arraycopy(binaryValue, 0, result, result.length - binaryValue.length, binaryValue.length);
        for (int i = 0; i < mask.length(); i++) {
            if (mask.charAt(i) != 'X') {
                result[i] = mask.charAt(i);
            }
        }
        return Long.parseLong(new String(result), 2);
    }
}
