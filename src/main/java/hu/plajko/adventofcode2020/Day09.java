package hu.plajko.adventofcode2020;

import java.util.List;
import java.util.Optional;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Day09 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day09.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        var lines = AdventUtils.readLines("day09.in");
        var numbers = lines.stream()
            .map(Long::parseLong)
            .collect(Collectors.toList());

        var part1 = solve1(numbers, 25);
        LOG.info("part1: {}", part1);
        LOG.info("part2: {}", solve2(numbers, part1.get()));
        LOG.info("part2: {}", solve2_stream(numbers, part1.get()));
    }

    private Optional<Long> solve1(List<Long> numbers, int windowSize) {
        return IntStream.range(0, numbers.size() - windowSize)
            .filter(i -> !IntStream.range(0, windowSize)
                .anyMatch(a -> IntStream.range(0, windowSize)
                    .filter(b -> a != b)
                    .anyMatch(b -> numbers.get(i + a) + numbers.get(i + b) == numbers.get(i + windowSize))))
            .mapToObj(i -> numbers.get(i + windowSize))
            .findFirst();
    }

    private long solve2(List<Long> numbers, long sumTarget) {
        int a = 0, b = 0;
        for (long sum = 0; b < numbers.size() && a <= b; sum += sum < sumTarget ? numbers.get(b++) : -numbers.get(a++)) {
            if (sum == sumTarget) {
                var range = new TreeSet<>(numbers.subList(a, b));
                return range.first() + range.last();
            }
        }
        return -1;
    }

    @With
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    static class State {
        int a = 0, b = 0;
        long sum = 0;

        boolean isValid(List<Long> numbers) {
            return b < numbers.size() && a <= b;
        }
        State next(List<Long> numbers, long sumTarget) {
            return sum < sumTarget
                ? this.withB(b + 1).withSum(sum + numbers.get(b))
                : this.withA(a + 1).withSum(sum - numbers.get(a));
        }
    }

    private Optional<Long> solve2_stream(List<Long> numbers, long sumTarget) {
        return Stream.iterate(
                new State(),
                state -> state.isValid(numbers),
                state -> state.next(numbers, sumTarget))
            .parallel()
            .filter(state -> state.getSum() == sumTarget)
            .findFirst()
                .map(state -> new TreeSet<>(numbers.subList(state.getA(), state.getB())))
                .map(range -> range.first() + range.last());
    }
}
