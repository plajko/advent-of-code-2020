package hu.plajko.adventofcode2020;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import com.google.common.base.Preconditions;
import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Day16 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day16.class, args);
    }

    @Data
    @AllArgsConstructor
    static class Range {
        long a;
        long b;

        boolean contains(long value) {
            return value >= a && value <= b;
        }
    }

    @Override
    public void run(String... args) throws Exception {
        var lines = AdventUtils.readLines("day16.in");
        var data = lines.stream()
            .collect(Collectors.toList());

        Multimap<String, Range> fieldRanges = MultimapBuilder.hashKeys().arrayListValues().build();
        int i = 0;
        while (!data.get(i).equals("")) {
            var s = data.get(i).split(": ");
            var ranges = Arrays.stream(s[1].split(" or "))
               .map(st -> st.split("-"))
               .map(sp -> new Range(Long.parseLong(sp[0]), Long.parseLong(sp[1])))
               .collect(Collectors.toList());
            fieldRanges.putAll(s[0], ranges);
            i++;
        }
        while (!data.get(i).equals("your ticket:")) {
            i++;
        }
        var myTicket = Arrays.stream(data.get(++i).split(","))
            .map(Long::parseLong)
            .collect(Collectors.toList());
        while (!data.get(i).equals("nearby tickets:")) {
            i++;
        }
        i++;
        var tickets = new ArrayList<List<Long>>();
        while (i < data.size()) {
            tickets.add(Arrays.stream(data.get(i).split(","))
                .map(Long::parseLong)
                .collect(Collectors.toList()));
            i++;
        }

        var part1Sum = tickets.stream().flatMap(List::stream)
            .filter(value -> fieldRanges.values().stream()
                .noneMatch(range -> range.contains(value)))
                .reduce(0L, (a, b) -> a + b);
        LOG.info("part1: {}", part1Sum);

        var validTickets = tickets.stream()
            .filter(ticket -> ticket.stream()
                .allMatch(value -> fieldRanges.values().stream()
                    .anyMatch(range -> range.contains(value))))
            .collect(Collectors.toList());

        Multimap<Integer, String> fieldCandidates = MultimapBuilder.hashKeys().arrayListValues().build();
        for (int j = 0; j < myTicket.size(); j++) {
            final int jf= j;
            var fieldValues = validTickets.stream()
                .map(ticket -> ticket.get(jf))
                .collect(Collectors.toList());
            var candidates = fieldRanges.asMap().entrySet().stream()
               .filter(e -> fieldValues.stream()
                   .allMatch(value -> e.getValue().stream()
                       .anyMatch(range -> range.contains(value))))
               .map(Entry::getKey)
               .collect(Collectors.toList());
            fieldCandidates.putAll(jf, candidates);
        }

        var matchedFields = new HashMap<String, Integer>();
        while (!fieldCandidates.values().isEmpty()) {
            var matchedField = fieldCandidates.asMap().entrySet().stream()
                .filter(e -> e.getValue().size() == 1)
                .findAny().get();
            var matchedFieldName = matchedField.getValue().stream().findAny().get();
            matchedFields.put(matchedFieldName, matchedField.getKey());

            Multimap<Integer, String> newCandidates = MultimapBuilder.hashKeys().arrayListValues().build();
            fieldCandidates.entries().stream()
                .filter(e -> !matchedFieldName.equals(e.getValue()))
                .forEach(e -> newCandidates.put(e.getKey(), e.getValue()));
            fieldCandidates = newCandidates;
        }
        var departureFields = matchedFields.entrySet().stream()
            .filter(e -> e.getKey().startsWith("departure"))
            .collect(Collectors.toList());
        Preconditions.checkState(departureFields.size() == 6);
        var part2Product = departureFields.stream()
            .mapToLong(field -> myTicket.get(field.getValue()))
            .reduce(1L, (a, b) -> a * b);
        LOG.info("part2: {}", part2Product);
    }

}
