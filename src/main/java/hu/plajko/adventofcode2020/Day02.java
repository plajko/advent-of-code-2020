package hu.plajko.adventofcode2020;

import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import com.google.common.base.Preconditions;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Day02 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day02.class, args);
    }

    @Data
    @AllArgsConstructor(staticName = "of")
    private static class Line {

        static final Pattern PATTERN = Pattern.compile("^(\\d+)-(\\d+) (.): (.*)$");

        long min;
        long max;
        int c;
        String pw;

        static Line parse(String s) {
            var m = PATTERN.matcher(s);
            Preconditions.checkArgument(m.matches());
            return of(
                Long.parseLong(m.group(1)),
                Long.parseLong(m.group(2)),
                m.group(3).charAt(0),
                m.group(4)
            );
        }

        boolean isValid1() {
            var index = pw.chars().boxed()
                .collect(Collectors.groupingBy(
                    Function.identity(),
                    Collectors.counting()));
            long count = Optional.ofNullable(index.get(c)).orElse(0L);
            return count >= min && count <= max;
        }

        boolean isValid2() {
            return pw.charAt((int) min - 1) == c
                 ^ pw.charAt((int) max - 1) == c;
        }
    }

    @Override
    public void run(String... args) throws Exception {
        var lines = AdventUtils.readLines("day02.in").stream()
            .map(Line::parse)
            .collect(Collectors.toList());
        LOG.info("part1: {}", lines.stream().filter(Line::isValid1).collect(Collectors.counting()));
        LOG.info("part2: {}", lines.stream().filter(Line::isValid2).collect(Collectors.counting()));
    }

}
