package hu.plajko.adventofcode2020;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.BiFunction;
import java.util.function.IntFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Day17 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day17.class, args);
    }

    @EqualsAndHashCode
    @NoArgsConstructor(staticName = "create")
    @AllArgsConstructor(staticName = "of")
    static class Point {
        private List<Long> coordinates = new ArrayList<>();

        Long getCoordinate(int dim) {
            return coordinates.get(dim);
        }
        Point add(long n) {
            return map(dim -> coordinates.get(dim) + n);
        }
        Point minus(Point other) {
            return map(dim -> coordinates.get(dim) - other.coordinates.get(dim));
        }
        private Point map(IntFunction<Long> dimMapper) {
            return Point.of(IntStream.range(0, coordinates.size())
                 .mapToObj(dimMapper)
                 .collect(Collectors.toList()));
        }
        Long size() {
            return coordinates.stream().reduce(1L, (a, b) -> a * b);
        }
        Point fill(int desiredSize, long value) {
            while (coordinates.size() < desiredSize) {
                coordinates.add(value);
            }
            return this;
        }
    }

    @Override
    public void run(String... args) throws Exception {
        var data = AdventUtils.readLines("day17.in");
        var timer = Stopwatch.createStarted();
        LOG.info("part1: {}", solve(data, 3, 6));
        LOG.info("part2: {}", solve(data, 4, 6));
        LOG.info("took {}", timer);
    }

    private long solve(List<String> data, int dimCount, int cycles) {
        var volumeIterator = volumeIterator(dimCount);

        var min = Point.create().fill(dimCount, 0L);
        var max = Point.of(Lists.newArrayList((long) data.size(), (long) data.get(0).length())).fill(dimCount, 1L);

        var startState = new ConcurrentHashMap<Point, Boolean>();
        volumeIterator.apply(min, max).forEach(point -> {
            var line = data.get(point.getCoordinate(0).intValue());
            char c = line.charAt(point.getCoordinate(1).intValue());
            startState.put(point, c == '#');
        });

        ConcurrentMap<Point, Boolean> state = new ConcurrentHashMap<>(startState);
        for (int i = 0; i < cycles; i++) {
            min = min.add(-1);
            max = max.add(1);
            state = nextState(state, min, max, volumeIterator);
        }
        return state.values().stream().filter(v -> v).count();
    }

    private ConcurrentMap<Point, Boolean> nextState(
            ConcurrentMap<Point, Boolean> state,
            Point min, Point max,
            BiFunction<Point, Point, Stream<Point>> iterator) {
        var result = new ConcurrentHashMap<Point, Boolean>();
        iterator.apply(min, max).forEach(point -> {
            var activeNeighbours = iterator.apply(point.add(-1), point.add(2))
                .filter(neighbour -> !neighbour.equals(point)) // not self
                .filter(neighbour -> Optional.ofNullable(state.get(neighbour)).orElse(false)) // active only
                .count();
            boolean isPointActive = Optional.ofNullable(state.get(point)).orElse(false);
            if (isPointActive) {
                result.put(point, activeNeighbours == 2 || activeNeighbours == 3);
            } else {
                result.put(point, activeNeighbours == 3);
            }
        });
        return result;
    }

    private static BiFunction<Point, Point, Stream<Point>> volumeIterator(int dimCount) {
        return (from, to) -> {
            var diff = to.minus(from);
            return LongStream.range(0, diff.size()).mapToObj(index -> {
                var coords = new ArrayList<Long>();
                long div = index;
                for (int dim = 0; dim < dimCount; dim++) {
                    coords.add(from.getCoordinate(dim) + div % diff.getCoordinate(dim));
                    div = div / diff.getCoordinate(dim);
                }
                return Point.of(coords);
            }).parallel();
        };
    }

}
