package hu.plajko.adventofcode2020;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.google.common.collect.Maps;
import com.google.common.io.Resources;
import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;

@UtilityClass
class AdventUtils {

    @SneakyThrows
    List<String> readLines(String resourceFileName) {
        return Files.readAllLines(Path.of(Resources.getResource(resourceFileName).toURI()), StandardCharsets.UTF_8);
    }

    <T> Collection<List<T>> split(List<T> list, Predicate<T> condition) {
        final var groupIdState = new AtomicInteger();
        return list.stream()
            .map(i -> Optional.of(condition.test(i))
                .map(test -> Maps.immutableEntry(
                    groupIdState.getAndAdd(test ? 1 : 0),
                    Optional.ofNullable(test ? null : i)
                )).get())
            .filter(e -> e.getValue().isPresent())
            .collect(Collectors.groupingBy(
                Entry::getKey,
                TreeMap::new,
                Collectors.mapping(
                    e -> e.getValue().get(),
                    Collectors.toList())
                ))
            .values();
    }

}
