package hu.plajko.adventofcode2020;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Day10 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day10.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        var lines = AdventUtils.readLines("day10.in");

        var data = lines.stream()
            .map(Long::parseLong)
            .collect(Collectors.toList());
        Collections.sort(data);
        data.add(0, 0L);
        data.add(data.size(), data.get(data.size() - 1) + 3);

        var count = IntStream.range(0, data.size() - 1)
            .mapToObj(i -> data.get(i + 1) - data.get(i))
            .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        LOG.info("part1: {}", count.get(1L) * count.get(3L));

        var tree = data.stream()
            .flatMap(a -> LongStream.rangeClosed(a + 1, a + 3)
                .filter(data::contains)
                .mapToObj(b -> new long[] { a, b }))
            .collect(Collectors.groupingBy(
                p -> p[0],
                Collectors.mapping(p -> p[1], Collectors.toSet())));
        LOG.info("part2: {}", countAll(tree, 0, new HashMap<>()));
    }

    private long countAll(Map<Long, Set<Long>> tree, long start, Map<Long, Long> cache) {
        if (cache.containsKey(start)) {
            return cache.get(start);
        }
        if (tree.containsKey(start)) {
            var result = tree.get(start).stream().mapToLong(i -> countAll(tree, i, cache)).sum();
            cache.put(start, result);
            return result;
        }
        return 1L;
    }

}
