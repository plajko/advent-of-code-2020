package hu.plajko.adventofcode2020;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import com.google.common.base.Preconditions;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Day11 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day11.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        var seats = AdventUtils.readLines("day11.in");
        LOG.info("part1: {}", settleAndCountOccupied(seats, 4, false));
        LOG.info("part2: {}", settleAndCountOccupied(seats, 5, true));
    }

    long settleAndCountOccupied(List<String> seats, int occupiedThreshold, boolean recursive) {
        var a = seats;
        var b = step(a, occupiedThreshold, recursive);
        while (!a.equals(b)) {
            a = b;
            b = step(a, occupiedThreshold, recursive);
        }
        return a.stream().flatMapToInt(String::chars).filter(c -> c == '#').count();
    }

    private List<String> step(List<String> seats, int occupiedThreshold, boolean recursive) {
        var newSeats = new ArrayList<String>();
        for (int i = 0; i < seats.size(); i++) {
            var row = seats.get(i);
            var newRow = new StringBuilder();
            for (int j = 0; j < row.length(); j++) {
                char seat = row.charAt(j);
                if ('.' == seat) {
                    newRow.append('.');
                    continue;
                }
                int[][] directions = new int[][] {
                    { 1, 1 },
                    { 1, 0 },
                    { 1, -1 },
                    { -1, 1 },
                    { -1, 0 },
                    { -1, -1 },
                    { 0, 1 },
                    { 0, -1 }
                };

                int x = i;
                int y = j;
                var occupiedSeen = Arrays.stream(directions)
                    .mapToInt(d -> checkDirection(seats, x, y, d[0], d[1], recursive))
                    .sum();

                if ('L' == seat) {
                    newRow.append(occupiedSeen == 0 ? '#' : 'L' );
                } else if ('#' == seat) {
                    newRow.append(occupiedSeen >= occupiedThreshold ?  'L' :'#');
                } else {
                    throw new IllegalStateException();
                }
            }
            newSeats.add(newRow.toString());
        }

//        newSeats.forEach(System.out::println);
//        System.out.println();
        return newSeats;
    }

    private int checkDirection(List<String> seats,
            int x, int y, int xInc, int yInc,
            boolean recursive) {
        int nx = x + xInc;
        int ny = y + yInc;
        var seat = getSeat(seats, nx, ny);
        if (seat == null || seat == 'L') {
            return 0;
        } else if (seat == '#') {
            return 1;
        }
        Preconditions.checkState(seat == '.');
        return recursive ? checkDirection(seats, nx, ny, xInc, yInc, true) : 0;
    }

    private Character getSeat(List<String> seats, int x, int y) {
        if (x < 0 || x >= seats.size() || y < 0 || y >= seats.get(0).length()) {
            return null;
        } else {
            return seats.get(x).charAt(y);
        }
    }

}
