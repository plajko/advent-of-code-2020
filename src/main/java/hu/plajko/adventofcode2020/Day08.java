package hu.plajko.adventofcode2020;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import lombok.AllArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Day08 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day08.class, args);
    }

    @Value
    @With
    @AllArgsConstructor(staticName = "of")
    static class Instruction {
        String operation;
        long argument;
    }

    @Value
    @AllArgsConstructor(staticName = "of")
    static class State {
        long acc;
        int lastLine;
        boolean terminated;
        int stepCount;
    }

    @Override
    public void run(String... args) throws Exception {
        var instructionPattern = Pattern.compile("^(.*) ([+-]\\d+)$");
        var code = AdventUtils.readLines("day08.in").stream()
            .map(instructionPattern::matcher)
            .map(m -> Optional.of(m).filter(Matcher::matches).orElseThrow())
            .map(m -> Instruction.of(m.group(1), Long.parseLong(m.group(2))))
            .collect(Collectors.toList());

        LOG.info("part1: {}", new Program(code).run());
        LOG.info("part2: {}", findError(code));
    }

    private Optional<State> findError(List<Instruction> code) {
        for (int line = 0; line < code.size(); line++) {
            var instruction = code.get(line);
            if ("nop".equals(instruction.getOperation())) {
                instruction = instruction.withOperation("jmp");
            } else if ("jmp".equals(instruction.getOperation())) {
                instruction = instruction.withOperation("nop");
            } else {
                continue;
            }
            var newCode = code.stream().collect(Collectors.toList());
            newCode.set(line, instruction);
            var result = new Program(newCode).run();
            if (result.isTerminated()) {
                return Optional.of(result);
            }
        }
        return Optional.empty();
    }

    static class Program {

        final List<Instruction> code;
        final Set<Integer> trace = new LinkedHashSet<>();

        int lastLine = 0;
        int nextLine = 0;
        long acc = 0;
        int stepCount = 0;

        Program(List<Instruction> code) {
            this.code = code;
        }

        State run() {
            while (step()) {
                stepCount++;
            }
            return state();
        }

        boolean step() {
            if (nextLine < 0 || nextLine >= code.size()) {
                return false;
            }
            if (trace.contains(nextLine)) {
                return false;
            }
            var instruction = code.get(nextLine);
            trace.add(nextLine);
            lastLine = nextLine;
            int jmp = 1;
            switch (instruction.getOperation()) {
                case "nop":
                    break;
                case "acc":
                    acc += instruction.getArgument();
                    break;
                case "jmp":
                    jmp = (int) instruction.getArgument();
                    break;
                default:
                    throw new IllegalStateException("Unsupported operation: " + instruction.getOperation());
            }
            nextLine += jmp;
            return true;
        }

        State state() {
            return State.of(
                acc,
                lastLine,
                nextLine == code.size(),
                stepCount);
        }
    }
}
