package hu.plajko.adventofcode2020;

import static java.lang.Integer.parseInt;

import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import com.google.common.base.Splitter;
import com.google.common.collect.Range;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Day04 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day04.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        var lines = AdventUtils.readLines("day04.in");
        var passports = AdventUtils.split(lines, String::isBlank).stream()
            .map(rp -> rp.stream()
                .flatMap(Splitter.on(" ")::splitToStream)
                .map(entry -> entry.split(":"))
                .collect(Collectors.toMap(e -> e[0], e -> e[1], (a, b) -> a, TreeMap::new)))
            .collect(Collectors.toList());

//        passports.forEach(p -> LOG.info("{}", p));

        var count1 = passports.stream()
            .filter(p -> p.keySet().containsAll(Set.of("hcl", "iyr", "hgt", "byr", "pid", "eyr", "ecl")))
            .collect(Collectors.counting());
        LOG.info("part1: {}", count1);

        var hgtPattern = Pattern.compile("(\\d+)(cm|in)");
        var count2 = passports.stream()
            .filter(p -> p.keySet().containsAll(Set.of("hcl", "iyr", "hgt", "byr", "pid", "eyr", "ecl")))
            .filter(p -> Range.closed(1920, 2002).contains(parseInt(p.get("byr"))))
            .filter(p -> Range.closed(2010, 2020).contains(parseInt(p.get("iyr"))))
            .filter(p -> Range.closed(2020, 2030).contains(parseInt(p.get("eyr"))))
            .filter(p ->
                Optional.of(hgtPattern.matcher(p.get("hgt")))
                    .filter(Matcher::matches)
                    .map(hgt -> (hgt.group(2).equals("cm")
                            ? Range.closed(150, 193)
                            : Range.closed(59, 76))
                        .contains(parseInt(hgt.group(1))))
                    .orElse(false)
            )
            .filter(p -> p.get("hcl").matches("#[0-9a-f]{6}"))
            .filter(p -> Set.of("amb", "blu", "brn", "gry", "grn", "hzl", "oth").contains(p.get("ecl")))
            .filter(p -> p.get("pid").matches("\\d{9}"))
            .collect(Collectors.counting());
        LOG.info("part2: {}", count2);
    }
}
