package hu.plajko.adventofcode2020;

import java.util.List;
import java.util.stream.Stream;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Day03 implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(Day03.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        var lines = AdventUtils.readLines("day03.in");
        LOG.info("part1: {}", solve(lines, 3, 1));

        long part2 = Stream.of(
            List.of(1, 1),
            List.of(3, 1),
            List.of(5, 1),
            List.of(7, 1),
            List.of(1, 2)
        )
            .mapToLong(p -> solve(lines, p.get(0), p.get(1)))
            .reduce(1L, (a, b) -> a * b);
        LOG.info("part2: {}", part2);

    }

    private long solve(List<String> lines, int x, int y) {
        var mod = lines.stream().findAny().map(String::length).orElse(0);
        long counter = 0;
        int index = 0;
        for (int line = 0; line < lines.size(); line += y) {
            if (lines.get(line).charAt(index) == '#') {
                counter++;
            }
            index = (index + x) % mod;
        }
//        LOG.info("{} * {} ({},{}): {}", lines.size(), mod, x, y, counter);
        return counter;
    }
}
